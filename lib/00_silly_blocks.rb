def reverser(&prc)
    returned_string = prc.call
    word = returned_string.split
    reversed_words = word.map {|ele| ele.reverse}
    reversed_words.join(' ')
end

def adder(num=1, &prc)
  prc.call + num
end

def repeater(num=1, &prc)
  num.times {prc.call}
end
