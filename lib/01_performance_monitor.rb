def measure (num=1, &prc)
  start_time = Time.now
  num.times {prc.call}
  end_time = Time.now
  (end_time - start_time)/num
end
